// $(window).resize(function ()// 绑定到窗口的这个事件中
// {
//   let designSize = 1920; // 设计图尺寸
//
//   let html = document.documentElement;
//
//   let wW = html.clientWidth;// 窗口宽度
//   let rem = wW * 100 / designSize;
//
//   document.documentElement.style.fontSize = rem + 'px';
// });


var deviceWidth
setHtmlFontSize()

if (window.addEventListener) {
  window.addEventListener('resize', function () {
    setHtmlFontSize()
  }, false)
}
function setHtmlFontSize () {
  // 1366是设计稿的宽度，当大于1366时采用1366宽度，比例也是除以13.66
  deviceWidth = document.documentElement.clientWidth > 1366 ? 1366 : document.documentElement.clientWidth
  document.getElementsByTagName('html')[0].style.cssText = 'font-size:' + deviceWidth / 13.66 + 'px !important'
}
