import Vue from 'vue'
import App from './App.vue'
import IView from 'iview'
import 'iview/dist/styles/iview.css'
Vue.use(IView);
import router from './routes'
import Service from './api'
Vue.use(Service)
import i18n from '@/lang'
Vue.config.productionTip = false
Vue.config.devtools = false
import store from './store'
import {
    fetch
} from '@/lib'
Vue.config.devtools = true
Vue.prototype.$fetch = fetch
const Bus = new Vue()
import $ from 'jquery'
import Element from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(Element, { size: 'small', zIndex: 3000 });
new Vue({
    data: {
        state: 0,
        Bus
    },
    router,
    store,
    i18n,
    render: h => h(App),
    mounted() {
        // this.$Loading.config({
        //   color: 'green',
        //   height: 5
        // });
    },
    beforeDestroy() {
        this.Bus.$off('updateData', this.getData);
    }
}).$mount('#app')
