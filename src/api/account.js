import {
    fetch
} from '@/lib'
import {
    APIURI
} from '@/config'
export default {
    //登录请求
    login({
        username,
        password,
        randomString
    }) {
        return fetch.post(APIURI.ACCOUNT.LOGIN, {
            username,
            password,
            randomString
        })
    }
}