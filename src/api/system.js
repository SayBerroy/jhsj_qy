import {
    fetch
} from '@/lib'
import {
    APIURI
} from '@/config'
export default {
    getHomeCount() {
        return fetch.get(APIURI.SYSTEM.COUNT)
    },
    getHomeBZSJ() {
        return fetch.get(APIURI.SYSTEM.BZSJ)
    },
    getHomeBZLXSJ() {
        return fetch.get(APIURI.SYSTEM.BZLXSJ)
    }
}