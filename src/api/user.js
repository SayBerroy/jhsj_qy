import {
    fetch
} from '@/lib'
import {
    APIURI
} from '@/config'
export default {
    getClassList() {
        return fetch.get(APIURI.USER.STUDENTLIST)
    }
}