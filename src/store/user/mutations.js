import {
    storage
} from '@/lib'
export default {
    logout(state) {
        state.nickname = ""
        storage.remove("userinfo");
        storage.remove("token");
        storage.remove("account");
    },
    setUserInfo(state, data) {
        state.compId = data.company;
    },
    // setActiveId(state, {
    //     menuid,
    //     labelid
    // }) {
    //     state.activeid = labelid
    //     state.openmenusid = [menuid]
    // }
    setActiveId(state,data){
        state.activeid = data.activeid;
    },
    setopenmenusId(state,data){
        state.openmenusid = data.openmenusid;
    }
}
