import state from './state'
import getters from './getters'
import mutations from './mutations'
import actions from './actions'
const store = {
    namespaced: true,
    getters,
    mutations,
    state,
    actions
}

export default store