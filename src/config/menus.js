export default [{
    label: "首页",
    path: "/",
    icon: "icon-home",
    inMenu: true,
    component: 'App.vue',
    sublist: [{
      path: "",
      label: "首页",
      inMenu: false,
      auth: true,
      component: 'Home.vue'
    }]
  }, {
    label: "特种设备信息",
    inMenu: true,
    icon: "icon-home1",
    path: "",
    component: 'App.vue',
    sublist: [{
        path: "inforManage",
        label: "企业基本信息",
        inMenu: true,
        component: 'inforManage.vue'
      }, {
        path: "equipment",
        label: "特种设备查询",
        inMenu: true,
        component: 'equipment.vue'
      }
      // ,{
      //   path: "qp",
      //   label: "气瓶",
      //   inMenu: true,
      //   component: 'list/qp.vue'
      // }
    ]
  },
  {
    label: "特种设备检验",
    inMenu: true,
    icon: "icon-home2",
    path: "",
    component: 'App.vue',
    sublist: [{
      path: "jyxx",
      label: "特种设备定期检验信息",
      inMenu: true,
      component: 'list/jyxx.vue'
    }, {
      path: "jyfj",
      label: "设备安全附件检验信息",
      inMenu: true,
      component: 'list/jyfj.vue'
    }]
  },
  {
    label: "作业人员信息",
    inMenu: true,
    icon: "icon-home3",
    path: "/zyry",
    component: 'App.vue',
    sublist: [
      //   {
      //   path: "aqglry",
      //   label: "设备安全管理人员信息",
      //   inMenu: true,
      //   component: 'list/aqglry.vue'
      // }, 
      {
        path: "",
        label: "设备作业人员信息",
        inMenu: false,
        component: 'list/zyry.vue'
      }
    ]
  }, {
    label: "安全主体责任",
    inMenu: true,
    icon: "icon-home4",
    path: "",
    component: 'App.vue',
    sublist: [{
      path: "gljg",
      label: "管理机构",
      inMenu: true,
      component: 'list/gljg.vue'
    }, {
      path: "zryy",
      label: "责任人员",
      inMenu: true,
      component: 'list/zryy.vue'
    }, {
      path: "gzzd",
      label: "规章制度",
      inMenu: true,
      component: 'list/gzzd.vue'
    }, {
      path: "yjya",
      label: "应急预案",
      inMenu: true,
      component: 'list/yjya.vue'
    }, {
      path: "sbyxjl",
      label: "设备运行记录",
      inMenu: true,
      component: 'list/sbyxjl.vue'
    }, {
      path: "gzwxjl",
      label: "设备故障和维修记录",
      inMenu: true,
      component: 'list/gzwxjl.vue'
    }, {
      path: "rypx",
      label: "人员培训记录",
      inMenu: true,
      component: 'list/rypx.vue'
    }]
  },
  //   {
  //   label: "锅炉视频监控",
  //   inMenu: true,
  //   icon: "icon-home3",
  //   path: "",
  //   component: 'App.vue',
  //   sublist: [{
  //     path: "equipment",
  //     label: "GIS显示",
  //     inMenu: true,
  //     component: 'equipment.vue'
  //   },
  //     {
  //       path: "equipment",
  //       label: "察看现场",
  //       inMenu: true,
  //       component: 'equipment.vue'
  //     },
  //     {
  //       path: "equipment",
  //       label: "未按照规定刷脸",
  //       inMenu: true,
  //       component: 'equipment.vue'
  //     }]
  // },
  {
    label: "监督检查管理",
    inMenu: true,
    icon: "icon-home6",
    path: "",
    component: 'App.vue',
    sublist: [{
        path: "JCTZ",
        inMenu: true,
        label: "检查通知",
        component: 'JCTZ.vue'
      },
      {
        path: "JCJG",
        inMenu: true,
        label: "监察指令书",
        component: 'JCJG.vue'
      },
      {
        path: "JCZLS",
        inMenu: true,
        label: "处罚决定书",
        component: 'JCZLS.vue'
      },
      // {
      //   path: "JCQK",
      //   inMenu: true,
      //   label: "其他",
      //   component: 'JCQK.vue'
      // }
    ]
  }, {
    label: "打卡数据",
    inMenu: true,
    icon: "icon-home6",
    path: "/clockData",
    component: 'App.vue',
    sublist: [{
      path: "",
      label: "打卡数据",
      inMenu: false,
      component: 'clockData.vue'
    }]
  },
  {
    label: "大屏",
    path: "/bigData",
    component: 'bigData.vue',
  }, {
    label: "登录",
    path: "/login",
    component: 'account/login.vue',
  },

  // {
  //   label: "用户管理",
  //   inMenu: true,
  //   icon: "icon-home5",
  //   path: "",
  //   component: 'App.vue',
  //   sublist: [{
  //     path: "User_user.vue",
  //     inMenu: true,
  //     label: "用户管理",
  //     component: 'BaiduMap/User_user.vue'
  //   },
  //     {
  //       path: "User_js",
  //       inMenu: true,
  //       label: "角色管理",
  //       component: 'BaiduMap/User_js.vue'
  //     },
  //     {
  //       path: "User_bm",
  //       inMenu: true,
  //       label: "部门管理",
  //       component: 'BaiduMap/User_bm.vue'
  //     },
  //     {
  //       path: "User_dl",
  //       inMenu: true,
  //       label: "登录日志",
  //       component: 'BaiduMap/User_dl.vue'
  //     },
  //   ]
  // },

  //   {
  //     label: "企业设备信息",
  //     inMenu: true,
  //     icon: "icon-home1",
  //     path: "/inforManage",
  //     component: 'App.vue',
  //     sublist: [{
  //             path: "",
  //             inMenu: false,
  //             label: "用电量检测",
  //             component: 'inforManage.vue'
  //         },
  //     ]
  // },
  //   {
  //     label: "视频监控",
  //     inMenu: true,
  //     icon: "icon-home2",
  //     path: "/SPJK",
  //     component: 'App.vue',
  //     sublist: [{
  //       path: "",
  //       inMenu: false,
  //       label: "用电量检测",
  //       component: 'SPJK.vue'
  //     },
  //     ]
  //   },
  // {
  //     label: "GIS",
  //     inMenu: true,
  //     icon: "icon-home1",
  //     path: "/GIS",
  //     component: 'App.vue',
  //     sublist: [{
  //             path: "",
  //             inMenu: false,
  //             label: "用电量检测",
  //             component: 'GIS.vue'
  //         },
  //     ]
  // },
  //   {
  //     label: "人脸识别系统",
  //     inMenu: true,
  //     icon: "icon-home2",
  //     path: "/sfyz",
  //     component: 'App.vue',
  //     sublist: [{
  //       path: "",
  //       label: "身份认证管理",
  //       inMenu: false,
  //       component: 'rl_sfrz'
  //     },
  //     ]
  //   },
  //
  // {
  //     label: "安全提醒",
  //     inMenu: true,
  //     icon: "icon-home5",
  //     path: "",
  //     component: 'App.vue',
  //     sublist: [{
  //             path: "SBDQ",
  //             inMenu: true,
  //             label: "设备检验提醒",
  //             component: 'SBDQ.vue'
  //         },
  //         {
  //             path: "CZZDQ",
  //             inMenu: true,
  //             label: "操作证到期提醒",
  //             component: 'CZZDQ.vue'
  //         }
  //     ]
  // },{
  //     label: "系统管理",
  //     inMenu: true,
  //     icon: "icon-home7",
  //     path: "",
  //     component: 'App.vue',
  //     sublist: [{
  //             path: "peopMange",
  //             inMenu: true,
  //             label: "人员管理",
  //             component: 'peopMange.vue'
  //         },{
  //             path: "RoleMan",
  //             inMenu: true,
  //             label: "角色管理",
  //             component: 'RoleMan.vue'
  //         },{
  //             path: "userMange",
  //             inMenu: true,
  //             label: "用户管理",
  //             component: 'userMange.vue'
  //         },{
  //             path: "RiZhiMange",
  //             inMenu: true,
  //             label: "日志管理",
  //             component: 'RiZhiMange.vue'
  //         },
  //         {
  //             path: "YeWuMange",
  //             inMenu: true,
  //             label: "业务日志",
  //             component: 'YeWuMange.vue'
  //         },
  //     ]
  // },
]
