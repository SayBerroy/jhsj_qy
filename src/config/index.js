export {
    APIURI
}
from './apiUri'
export const lang = "cn"
export const baseURL = "/jeecg-boot"
import routes from './menus'
export const allRoutes = routes
    //所有菜单项
export const menuList = routes.filter(r => r.inMenu)
